<?php
class Category extends ModelTable {
	static $table = 'category';
	public $safe = array('id', 'url', 'title');

	public $articles = array();
}