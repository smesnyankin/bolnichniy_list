<?php

class IndexController extends Controller {
    
    function actionIndex($urlq = '') {
        if($urlq != '/'){
            $c = strlen($urlq)-1;
            $url = substr($urlq,-$c);
            unset($c);
            
            $product = Cpravka::modelWhere('url = ? ', array($url));
            if($product){
                $posts = array();
                switch ($product->page_type) {
                    case ('static'): {
                            $static = Cpravka::modelWhere('url = ? AND page_type = ?', array($url,'static'));
                            $this->meta_title = $static->meta_title;
                            $this->meta_keywords = $static->meta_keywords;
                            $this->meta_description = $static->meta_description;                            
                            
                            switch($static->url){
                                case ('cont.html'):{                         
                                    
                                    $this->setBreadCrumbs('/', 'Главная');
                                    $this->setBreadCrumbs('#', 'Контакты');
                                    $this->render('contact', array('page' => $static));
                                    break;
                                }
                                case ('obrazec.html'):{
                                    
                                    $this->setBreadCrumbs('/', 'Главная');
                                    $this->setBreadCrumbs('#', 'Образец');
                                    $this->render('contact', array('page' => $static));
                                    break;
                                }
                                case ('zakaz.html'):{
                                    
                                    $this->setBreadCrumbs('/', 'Главная');
                                    $this->setBreadCrumbs('#', 'Информация по заказу');
                                    $this->render('contact', array('page' => $static));
                                    break;
                                }
                                case ('price.html'):{
                                    
                                    $this->setBreadCrumbs('/', 'Главная');
                                    $this->setBreadCrumbs('#', 'Цены');
                                    $this->render('contact', array('page' => $static));
                                    break;
                                }
                            }
                        } break;
                    case ('product'): {
                            $prod = Cpravka::modelWhere('url = ? AND page_type = ?', array($url,'product'));
                            $form = Field::modelsWhere('id_spravka = ?', array($prod->id));
                            
                            $this->meta_title = $prod->meta_title;
                            $this->meta_keywords = $prod->meta_keywords;
                            $this->meta_description = $prod->meta_description;
                            
                            $this->setBreadCrumbs('/', 'Главная');
                            $this->setBreadCrumbs('#', $prod->price_title);
                            
                            $this->render('product', array('page' => $prod,'form'=>$form));
                        } break;
                    case ('product_dub'): {
                            $proddub = Cpravka::modelWhere('url = ? AND page_type = ?', array($url,'product_dub'));
                            $form = Field::modelsWhere('id_spravka = ?', array($proddub->id));
                            
                            $this->meta_title = $proddub->meta_title;
                            $this->meta_keywords = $proddub->meta_keywords;
                            $this->meta_description = $proddub->meta_description;
                            
                            $this->setBreadCrumbs('/', 'Главная');
                            $this->setBreadCrumbs('/reviews.html', 'Отзывы');
                            $this->setBreadCrumbs('#', $proddub->price_title);
                            
                            $this->render('productdub', array('page' => $proddub,'form'=>$form));
                        } break;
                    case ('qa_main'):                            
                    case ('qa_page'): {
                            $page = Cpravka::modelWhere('url = ? and page_type = ?', array($url,'qa_page'));
                            $parent = Cpravka::model($page->id_qa);
                            $pages = Cpravka::modelsWhere('id_qa = ? AND page_type = ?', array($parent->id,'qa_page'));
                            
                            $posts = Post::modelsWhere('id_page = ?', array($page->id));
                            $count = count($pages);
                            
                            $this->meta_title = $page->meta_title;
                            $this->meta_keywords = $page->meta_keywords;
                            $this->meta_description = $page->meta_description;
                            
                            $this->setBreadCrumbs('/', 'Главная');
                            $this->setBreadCrumbs('/news.html', 'Вопросы и ответы');
                            $this->setBreadCrumbs('#', $page->page_name);
                            $this->render('qa', array('posts' => $posts,'pages'=>$pages,'count'=>$count,'page'=>$page));
                        } break;
                    case ('review_page'): {
                            $page = Cpravka::modelWhere('url = ? AND page_type = ?', array($url,'review_page'));
                            $pages = Cpravka::modelsWhere('page_type = ?', array('review_page'));
                            
                            $posts = Post::modelsWhere('id_page = ?', array($page->id));
                            $count = count($pages);
                            
                            $this->meta_title = $page->meta_title;
                            $this->meta_keywords = $page->meta_keywords;
                            $this->meta_description = $page->meta_description;
                            
                            $this->setBreadCrumbs('/', 'Главная');
                            $this->setBreadCrumbs('#', $page->page_name);
                            $this->render('reveiw', array('posts' => $posts,'page'=>$page,'pages'=>$pages));
                        } break;
                    case ('mi_page'): {
                            $page = Cpravka::modelWhere('url = ? AND page_type = ?', array($url,'mi_page'));
                            $pages = Cpravka::modelsWhere('page_type = ?', array('mi_page'));
                            
                            $posts = Post::modelsWhere('id_page = ?', array($page->id));
                            $count = count($pages);
                            
                            $this->meta_title = $page->meta_title;
                            $this->meta_keywords = $page->meta_keywords;
                            $this->meta_description = $page->meta_description;
                            
                            $this->setBreadCrumbs('/', 'Главная');
                            $this->setBreadCrumbs('#', 'Мед. информация');
                            $this->render('mi', array('posts' => $posts,'page'=>$page,'pages'=>$pages));
                        } break;
                }
            }else{
                $search = explode('/', $url);
                if(count($search)>1){
                    if($post = Post::modelWhere('go_url = ?', array($url))){
    //                    $this->meta_title = $page->meta_title;
    //                    $this->meta_keywords = $page->meta_keywords;
    //                    $this->meta_description = $page->meta_description;
                        $this->setBreadCrumbs('/', 'Главная');
                        $this->setBreadCrumbs('/inform', 'Полезная информация');
                        $this->setBreadCrumbs('#', $post->question);

                        $this->render('article', array('post' => $post));
                    }else{
                        if($search[1] % 20 == 0){
//                            debug($search);
                            $posts = Post::modelsWhere('id LIMIT ?,20', array($search[1]));
                            if(count($posts)>0){
                                $countPosts = Post::countRow();
                                $pages = $countPosts/20;

                                $this->meta_title = 'Полезная инфомация';
                                $this->meta_keywords = 'Медицинская справка 095';
                                $this->meta_description = 'Медицинская справка 095 являет собою документ, подтверждающий временную нетрудоспособность ученика, студента или рабочего';

                                $this->setBreadCrumbs('/', 'Главная');
                                $this->setBreadCrumbs('#', 'Мед. информация');

                                $this->render('inform', array('posts' => $posts,'pages'=>$pages, 'active'=>$search[1]));
                            }else{
                                header("HTTP/1.x 404 Not Found");
                                $this->render('404');
                            }
                        }else{
                                header("HTTP/1.x 404 Not Found");
                                $this->render('404');
                            }
                    }
                }else{
                    switch ($url){
                        case ('inform'): {
    //                        $categories = Category::models();
    //                        if (count($categories)) {
    //                            foreach($categories as $category){
    //                                $list[$category->title] = Cpravka::modelsWhere('page_type = ? AND id_category = ?', array('product', $category->id));
    //                            }
    //                        }
                            $list = Post::modelsWhere('id LIMIT 20');
                            $countPosts = Post::countRow();
                            $pages = $countPosts/20;
                            
                            $this->meta_title = 'Полезная инфомация';
                            $this->meta_keywords = 'Медицинская справка 095';
                            $this->meta_description = 'Медицинская справка 095 являет собою документ, подтверждающий временную нетрудоспособность ученика, студента или рабочего';
                            
                            $active = 0;
                            
                            $this->setBreadCrumbs('/', 'Главная');
                            $this->setBreadCrumbs('#', 'Полезная информация');
                            $this->render('inform', array('posts'=>$list, 'pages'=>$pages, 'active'=>$active));
                            break;
                        }
                        case ('news.html'): {
                            $list = Cpravka::modelsWhere('page_type = ?', array('qa_main'));

                            $this->meta_title = 'Вопросы посетителей сайта и ответы на них от специалистов';
                            $this->meta_keywords = '';
                            $this->meta_description = '';

                            $this->setBreadCrumbs('/', 'Главная');
                            $this->setBreadCrumbs('#', 'Вопросы и ответы');
                            $this->render('qamain', array('list'=>$list));
                            break;
                        }
                        case ('form.html'): {
                                $this->render('writeus');
                                break;
                        }
                        case ('usform'): {
                                $this->actionReviewForm();
                                break;
                        }
                        case ('rform'): {
                                $this->actionReviewForm();
                                break;
                        }
                        case ('pform'): {
                                $this->actionProductForm();
                                break;
                        }
                        case ('dubform'): {
                                $this->actionDubForm();
                                break;
                        }
                        default:{
                            header("HTTP/1.x 404 Not Found");
                            $this->render('404');
                            break;
                        }
                    }
                }
            } 
        }else{
            //Главная страница сайта
//            $categories = Category::models();
//            if (count($categories)) {
//                foreach($categories as $category){
//                    $list[$category->title] = Cpravka::modelsWhere('page_type = ? AND id_category = ?', array('product', $category->id));
//                }
//            }
//            
            $this->meta_title = 'Больничный лист купить в Москве больничные листы бесплатно консультируем';
            $this->meta_keywords = 'Мы изготавливаем для клиентов больничные листы';
            $this->meta_description = 'Оформление больничных листов! Узнай, как купить больничный лист в городе Москва, позвонив по номеру 8(926)052-97-83';
                            
            $this->render('index');//, array('list'=>$list));
        }
    }
    
    public function actionProductForm(){
        if(isset($_POST['fio'])){
            $message = '';
            $msg = '';
            $titles = array();
            $urls = array();
            $allsprvk = Cpravka::modelsWhere('page_type = ?', array('product'));
            
            foreach($allsprvk as $sprvk){
                    $form = Field::modelsWhere('id_spravka = ?', array($sprvk->id));
                    foreach($form as $field){
                        if(!in_array($field->url,$urls)){
                            $titles[] = $field->title; 
                            $urls[] = $field->url; 
                        }
                    }
            }
            
            $to  = 'spravka.com@yandex.ru, pavellazarev01@gmail.com, dmitriybinovery@gmail.com, apmagedon2@gmail.com';
//            $email_admin= 'info@spravki-v-moskve.com'; 
            $codepage = 'utf8';
            $subject = 'Запрос на покупку справки на cpravka.com';
            
//            if(isset($_POST['data_rojdeniya'])){
//                $dr = explode('-',$_POST['data_rojdeniya']);
//                if((int)$dr[0]<1900){
//                    $this->redirect($_SERVER['HTTP_REFERER']);
//                }
//            }
            
            $c = count($titles);
            for($i=0;$i<$c;$i++){
                if(isset($_POST["$urls[$i]"])){
                    $msg.= "$titles[$i] ".$_POST["$urls[$i]"]."\r\n";
                }
            }
            unset($c);
            
            if(isset($_POST['metro'])){
                 $msg.= "Нужна ли доставка до станции метро: ".$_POST["metro"]."\r\n";
            }
            if(isset($_POST['comments'])){
                 $msg.= "Комментарий: ".$_POST["comments"]."\r\n";
            }
            
            $headers = "Content-Type: text/html; charset=$codepage\r\n";
//            $headers .= "From: $email_admin" . "\r\n";
            $headers .= "X-Mailer: My Send E-mail\r\n"; 
            
            if(mail($to, $subject, $msg)){
                $message = "Спасибо, Ваша заявка принята. Мы свяжемся с Вами в ближайшее время. Через 5 секунд вы будете перенапрвлены на страницу с которой пришли.";	
            } else {
                $message = "Не удалось отправить сообщение. Через 5 секунд вы будете перенапрвлены на страницу с которой пришли.";
            }

            $this->render('form', array('message'=>$message,'msg'=>$msg));
        }
    }
    
    public function actionDubForm(){
        if(isset($_POST['fio'])){
            $message = '';
            $msg = '';
            $titles = array();
            $urls = array();
            $allsprvk = Cpravka::modelsWhere('page_type = ?', array('product_dub'));
            
            foreach($allsprvk as $sprvk){
                    $form = Field::modelsWhere('id_spravka = ?', array($sprvk->id));
                    foreach($form as $field){
                        if(!in_array($field->url,$urls)){
                            $titles[] = $field->title; 
                            $urls[] = $field->url; 
                        }
                    }
            }
            
            $to  = 'spravka.com@yandex.ru, pavellazarev01@gmail.com, dmitriybinovery@gmail.com, apmagedon2@gmail.com';
//            $email_admin= 'info@spravki-v-moskve.com'; 
            $codepage = 'utf8';
            $subject = 'Запрос на покупку справки на cpravka.com';
            
//            if(isset($_POST['data_rojdeniya'])){
//                $dr = explode('-',$_POST['data_rojdeniya']);
//                if((int)$dr[0]<1900){
//                    $this->redirect($_SERVER['HTTP_REFERER']);
//                }
//            }
            
            $c = count($titles);
            for($i=0;$i<$c;$i++){
                if(isset($_POST["$urls[$i]"])){
                    $msg.= "$titles[$i] ".$_POST["$urls[$i]"]."\r\n";
                }
            }
            unset($c);
            
            if(isset($_POST['metro'])){
                 $msg.= "Нужна ли доставка до станции метро: ".$_POST["metro"]."\r\n";
            }
            if(isset($_POST['comments'])){
                 $msg.= "Комментарий: ".$_POST["comments"]."\r\n";
            }
            
            $headers = "Content-Type: text/html; charset=$codepage\r\n";
//            $headers .= "From: $email_admin" . "\r\n";
            $headers .= "X-Mailer: My Send E-mail\r\n"; 
            
            if(mail($to, $subject, $msg)){
                $message = "Спасибо, Ваша заявка принята. Мы свяжемся с Вами в ближайшее время. Через 5 секунд вы будете перенапрвлены на страницу с которой пришли.";	
            } else {
                $message = "Не удалось отправить сообщение. Через 5 секунд вы будете перенапрвлены на страницу с которой пришли.";
            }

            $this->render('form', array('message'=>$message,'msg'=>$msg));
        }
    }
    
    public function actionReviewForm(){
        if(isset($_POST['name'])){
            $message = '';
            $msg = '';
            
            $to  = 'spravka.com@yandex.ru, pavellazarev01@gmail.com, dmitriybinovery@gmail.com, apmagedon2@gmail.com';
//            $email_admin= 'info@spravki-v-moskve.com'; 
            $codepage = 'utf8';
            $subject = 'Запрос на добавление отзыва на cpravka.com';
            
            $msg .= "Имя: ${_POST['name']}";
            $msg .= "Тема: ${_POST['theme']}";
            $msg .= "Email: ${_POST['email']}";
            $msg .= "Сообщение: ${_POST['message']}";
            
            $headers = "Content-Type: text/html; charset=$codepage\r\n";
//            $headers .= "From: $email_admin" . "\r\n";
            $headers .= "X-Mailer: My Send E-mail\r\n"; 
            
            if(mail($to, $subject, $msg)){
                $message = "Спасибо, Ваша заявка принята. Мы свяжемся с Вами в ближайшее время. Через 5 секунд вы будете перенапрвлены на страницу с которой пришли.";	
            } else {
                $message = "Не удалось отправить сообщение. Через 5 секунд вы будете перенапрвлены на страницу с которой пришли.";
            }

            $this->render('form', array('message'=>$message,'msg'=>$msg));
        }
    }

}
