<?php
class SpravkaController extends Controller {
    public function actionView($url = ''){
        if(!empty($url)){
            $spravka = Spravka::modelWhere('url = ?', array($url));
            if($spravka){
                $fields = Field::modelsWhere('id_spravka = ?', array($spravka->id));
                $this->render('view', array('spravka' => $spravka, 'fields' => $fields));
                return;
            }
        }
        $this->redirect('/');
    }
}

