<?php

class ErrorController extends Controller {

    function actionIndex() {
        $this->redirect('/error/404');
    }

    function action404() {
        $this->meta_title = '404';
        $this->render('404');
    }
}
