<?php

return array(
    'sitename' => 'cpravka.com',
    'db' => include 'config.db.php',
    'layout' => 'base',
    'path_error_controller' => '/error', //урла контроллера ошибки
    'router' => array(
        'rform' => 'index/reviewform',
        'pform' => 'index/productform',
        '([a-z0-9+_\-]+)' => 'index/index/$id',
//        '([a-z0-9+_\-]+)/([a-z0-9+_\-]+)/([a-z0-9+_\-]+)' => '$controller/$action/$id',
//        '([a-z0-9+_\-]+)/([a-z0-9+_\-]+)' => '$controller/$action',
//        '([a-z0-9+_\-]+)/?' => '$controller',
//        '([a-z0-9+_\-]+)/([a-z0-9+_\-]+)/([a-z0-9+_\-]+)/.html' => '$controller/$action/$id',
//        'o-kompanii' => 'index/about',
//        'stati' => 'index/articles',
//        // 'katalog' => 'index/catalog',
//        'index/index/([a-z0-9+_\-]+)' => '$controller/$action',
//        'oplata' => 'index/pay',
//        'dostavka' => 'index/delivery',
//        'kontakty' => 'index/contacts',
//        'vopros-otvet' => 'index/faq',
//        'stati/([a-z0-9+_\-]+)' => 'index/read/$id',
//        'cpravka/([a-z0-9+_\-]+)' => 'cpravka/view/$id',
    // 'register' => 'user/register',
    ),
);
