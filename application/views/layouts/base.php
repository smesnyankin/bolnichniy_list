
    <?php 
    $class = 'style_header';
    $classFooter = 'page';
        if($_SERVER['REQUEST_URI'] == '/') {
            $class = 'home';
            $classFooter = '';
        }

     ?>

    <header class="<?= $class; ?> ">
        <div class="container">
            <div class="row">
                <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2">
                    <div class="logo">
                        <a href="/"><img src="/assets/img/logo.png" alt=""></a>
                    </div>
                </div>
                <div class="col-xs-2 col-xs-offset-6 col-sm-2 col-md-7 col-md-offset-0 col-lg-7">
                    <div class="drop-menu">
                            <span class="first"></span>
                            <span class="second"></span>
                            <span class="third"></span>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                    <nav>
                        <ul>
                            <li><a href="/">Главния</a></li>
                            <li><a href="/obrazec.html">Образец</a></li>
                            <li><a href="/price.html">Цены</a></li>
                            <li><a href="/inform">Информация</a></li>
                            <li><a href="/cont.html">Контакты</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-8 col-sm-8 col-sm-offset-2 col-md-3 col-md-offset-0 col-lg-3">
                    <div class="contacts">
                        <div class="data_contacts">
                            <span>8 (067) 269 14 84</span>
                            <p>Без выходных, с <span>8:00</span> до <span>00:00</span></p>
                        </div>
                        <button data-toggle="modal" data-target="#myModal">Заказать сейчас</button>
                    </div>
                </div>
            </div>

            <?php  

            if($_SERVER['REQUEST_URI'] == '/') {


            ?>
            
                <div class="row">
                    <div class="col-xs-12  col-sm-11 col-sm-offset-1 col-md-11 col-md-offset-1 col-lg-11 col-lg-offset-1">
                        <div class="main_text">
                            <h2>Броский заголовок, о больничных листах</h2>
                            <p>
                                Какое либо описание по теме, например В жизни бывают непредвиденные ситуации, выход из которых необходимо искать быстро и без лишних хлопот. Например, вам необходимо как-то
                            </p>
                        </div>
                        <div class="term">
                            <span>1 День</span>
                            <p>Быстрое<br>оформление</p>
                        </div>
                        <div class="cost">
                            <span>От 1500 руб</span>
                            <p>Приемлемая<br>стоимость</p>
                        </div>
                        <div class="up_bottom">
                            <a href="#linc"><i class="fa fa-angle-down"></i></a>
                        </div>
                        <div class="angle">
                            <i class="fa fa-angle-down"></i>
                            <i class="fa fa-angle-down"></i>
                        </div>
                    </div>
                </div>

           
            <?php  } ?>
        </div>
    </header>
    <div class="content">
        <div class="container">
            <?=$content; ?>
        </div>
    </div>
    <footer class="<?= $classFooter; ?>">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-md-4 col-lg-5">
                    <div class="logo">
                        <a href="/"><img src="/assets/img/logo.png" alt=""></a>
                    </div>

                    <nav>
                    <p>Навигация</p>
                        <ul>
                            <li><a href="/">Главния</a></li>
                            <li><a href="/obrazec.html">Образец</a></li>
                            <li><a href="/price.html">Цены</a></li>
                            <li><a href="/vashu-voprosu">Информация</a></li>
                            <li><a href="/cont.html">Контакты</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-7">
                    <div class="map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4496.581248284965!2d37.87069113338063!3d55.70131991874874!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x414ac9fdf648796f%3A0xb463efd28f59fe54!2z0YPQuy4g0JrQsNC80L7QstCwLCAxMiwg0JzQvtGB0LrQstCwLCDQoNC-0YHRgdC40Y8sIDExMTYyNQ!5e0!3m2!1sru!2sua!4v1454329193934" width="100%" height="200" frameborder="0" style="border-radius:5px" allowfullscreen></iframe>
                    </div>
                    <div class="contacts">
                        <p>Контакты</p>
                        <p class="location">г. Москва, ул. Камова 12, 111625</p>
                        <p class="mail">bolnichnie-listy@mail.ru</p>
                        <p class="phone">8 (965) 106 16 60</p>
                        <button data-toggle="modal" data-target="#myModal">Заказать сейчас</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright">
            <p>Copyright 2011-2012 bolnichnie-listy.ru</p>
        </div>
        <div class="modal fade" id="myModal">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h6 class="modal-title">Заказать больничный лист</h6>
              </div>
              <div class="modal-body">
                <form action="">
                    <p>Как Вас зовут?
                        <input type="text" name="text" placeholder="Никола Питерский" required pattern="^[А-Яа-яЁё][А-Яа-яЁё0-9\s]{1,20}$">
                    </p>
                    <p>Телефон
                        <input type="text" id="phone" class="phone" name="phone" placeholder="+7 (___) ___-__-__" required>
                    </p>
                    <p class="text">Message</p>
                    <textarea cols="20" rows="7" placeholder="Коротко не получится..."></textarea>
                    <button type="submit"><i class="fa fa-chevron-right"></i>Отправить<i class="fa fa-chevron-left"></i></button>
                </form>
              </div>
              <div class="modal-footer">
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </footer>
    <div></div>
    <div class="wrap-bottom">
        <div class="go-up" title="Top" id='ToTop' ><i class="fa fa-chevron-circle-up"></i></div>
        <div class="go-down" title="Bottom" id='OnBottom'><i class="fa fa-chevron-circle-down"></i></div>
    </div>
