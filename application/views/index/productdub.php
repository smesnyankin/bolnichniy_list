<?php if ($page) { ?>
<h1><?=$page->page_name; ?></h1>
<p><?= $page->price_title; ?></p>
<div class="imageblock">
    <img src="/uploads/<?=$page->id.'/'.$page->img; ?>" width ="375" >
    <div class="price">Цена: <?= $page->price; ?></div>
    <p class="delivery-today">Доставим сегодня!</p>
</div>
<div class="order">
    <span class="title">Заказать онлайн</span>
    <form action="/pform" method="post" class="validate">
        <div class="inputs">
        <?php foreach ($form as $field){ 
            if($field->url !=='okrug_projivaniya' and $field->url !=='data_rojdeniya'){ ?>        
                
                <label>
                   <?=$field->title; ?>
                    <input type="text" name="<?=$field->url; ?>"  class="required">
                </label>
            <?php }elseif($field->url ==='okrug_projivaniya'){ ?>
                <label>
                    <?=$field->title; ?>
                    <select name="<?=$field->url; ?>" class="required">
                        <option value="ЦАО">ЦАО</option>
                        <option value="ЗАО">ЗАО</option>
                        <option value="САО">САО</option>
                        <option value="ВАО">ВАО</option>
                        <option value="ЮАО">ЮАО</option>
                        <option value="СЗАО">СЗАО</option>
                        <option value="СВАО">СВАО</option>
                        <option value="ЮВАО">ЮВАО</option>
                        <option value="ЮЗАО">ЮЗАО</option>
                        <option value="Зеленоград">Зеленоград</option>
                    </select>
                </label>
            <?php }else{ ?>
                <label>
                    <?=$field->title; ?>
                    <input type="date" name="<?=$field->url; ?>" class="datepicker"  class="required">
                </label>
            <?php } 
        } ?>
            <label>
                Требуется ли доставка к станции метро?<br>
                <select name="metro" class="required">
                    <option value="Выберите вариант" style="color:#ccc !important;" disabled selected></option>
                    <option value="Да">Да</option>
                    <option value="Нет">Нет</option>
                </select>
            </label>
            <label>
                Комменты:
            <textarea name="comments"  class="required"></textarea>
            </label>
        </div>
        <div class="submit">
            <input type="submit" value="ОФОРМИТЬ ЗАКАЗ" class="send">
        </div>
    </form>
</div>
<p><?=$page->page_content; ?></p>
<?php } ?>

