<?php if ($page) { ?>
<h1><?=$page->page_name; ?></h1>
<p><?= $page->price_title; ?></p>
<img src="/uploads/<?=$page->id.'/'.$page->img; ?>" width ="375" >
<div class="order">
    <span class="title">Заказать онлайн</span>
    <form action="/dubform" method="post">
        <div class="inputs">
        <?php foreach ($form as $field){ 
            if($field->url !=='okrug_projivaniya' and $field->url !=='data_rojdeniya'){ ?>        
                <?=$field->title; ?>
                <input type="text" name="<?=$field->url; ?>" required>
            <?php }elseif($field->url ==='okrug_projivaniya'){ ?>
                <?=$field->title; ?>
                <select name="<?=$field->url; ?>">
                    <option value="ЦАО">ЦАО</option>
                    <option value="ЗАО">ЗАО</option>
                    <option value="САО">САО</option>
                    <option value="ВАО">ВАО</option>
                    <option value="ЮАО">ЮАО</option>
                    <option value="СЗАО">СЗАО</option>
                    <option value="СВАО">СВАО</option>
                    <option value="ЮВАО">ЮВАО</option>
                    <option value="ЮЗАО">ЮЗАО</option>
                    <option value="Зеленоград">Зеленоград</option>
                </select>
            <?php }else{ ?>
                <?=$field->title; ?>
                <input type="date" name="<?=$field->url; ?>" class="datepicker" required>
            <?php } 
        } ?>
            Требуется ли доставка к станции метро?<br>
            <select name="metro">
                <option value="Да">Да</option>
                <option value="Нет">Нет</option>
            </select>
            Комменты:
            <textarea name="comments" required></textarea>
        </div>
        <div class="submit">
            <input type="submit" value="отправить" class="send">
        </div>
    </form>
</div>
<p><?=$page->page_content; ?></p>
<?php } ?>

