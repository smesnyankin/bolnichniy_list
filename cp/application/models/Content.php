<?php
class Content extends ModelTable {
	static $table = 'content';
	public $safe = array('id', 'type', 'id_category', 'url', 'title', 'meta_title', 'meta_keywords', 'meta_description', 'content', 'mod_time');
}