<?php
class TestController extends Controller{
    static $rules = array(
            'index' => array(
                'users' => array('guest'),
                'redirect' => '/cp/index'),
            'qa' => array(
                'users' => array('admin'),
                'redirect' => '/cp/index')
        );
    public function actionIndex(){
        
    }
    
    public function actionQA(){
        $existpost = null;
        $pageadd = null;
        $last = null;
        $message = '';
        if (isset($_POST['form'])) {
            $post = new Post();
            $post->__attributes = $_POST['form'];
            if($_POST['form']['id_page'] != -1){
                if ($post->save()) {
                    $existpost = Post::modelsWhere('id_page = ?', array($post->id_page));
                    $pageadd = Cpravka::modelWhere('id = ?', array($post->id_page));
                    $last = &$post;
                }
            } else {
                $message = "Выберите страницу на которой будет размещен вопрос-ответ";
            }
        }
        $pages = Cpravka::modelsWhere('page_type = ?', array('qa_page'));

        $this->render('test', array('pages'=>$pages,'exists'=>$existpost, 'pageadd'=>$pageadd, 'last'=>$last,'message'=>$message));

    }
}