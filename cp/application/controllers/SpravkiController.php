<?php

class SpravkiController extends Controller {

    static $rules = array(
        'index' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'create' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'edit' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'editdublicate' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'delete' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'additional' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'editadditional' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'deleteadditional' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'price' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'createdublicate' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'viewdublicate' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login')
    );

    public function actionIndex() {
        $spravki = Cpravka::modelsWhere('page_type = ?', array('product'));
        $this->render('index', array('spravki' => $spravki));
    }
    
    public function actionViewDublicate() {
        $dubs = Cpravka::modelsWhere('page_type = ?', array('product_dub'));
        $this->render('viewdublicate', array('dubs' => $dubs));
    }
    
    public function actionPrice(){
            $prices = Cpravka::modelsWhere('page_type = ?', array('product'));
            $this->render('view', array('prices'=>$prices));
        }

    public function actionCreate() {
        if (isset($_POST['form'])) {
            $product = new Cpravka();
            $product->__attributes = $_POST['form'];
            $product->page_type = 'product';
            if ($product->save()) {
                $this->redirect('/cp/spravki/additional/' . $product->id);
            }
        }
        $categories = Category::modelsWhere('id ORDER BY id DESC');
        $this->render('create', array('categories' => $categories));
    }
    
    public function actionCreateDublicate(){
        if (isset($_POST['form'])) {
            $product = new Cpravka();
            $product->__attributes = $_POST['form'];
            $product->page_type = 'product_dub';
            if ($product->save()) {
                $this->redirect('/cp/spravki/additional/' . $product->id);
            }
        }
        $this->render('createdublicate');
    }

    public function actionLoadImage($id_spravka = 0) {
        $spravka = Cpravka::model($id_spravka);
        if ($spravka) {
            $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/uploads/';
            if(!empty($catalog)){
                $uploaddir .= $catalog . '/';
            }
            if(!file_exists($uploaddir)){
                mkdir($uploaddir);
            }
            $uploadfile = $uploaddir . basename($_FILES['userfile']['name']);

            if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
                return $_FILES['userfile']['name'];
            } else {
                return false;
            }
        }
        $this->render('loadImage');
    }

    public function actionAdditional($id = 0, $id_field = 0) {
        $product = Cpravka::model((int) $id);
        $fields = array();
        $field = new Field();
        if ($product) {
            
            if (isset($_FILES['userfile'])) {
                $img = $this->loadImage($product->id);
                if($img){
                    $product->img = $img;
                    $product->save();
                    $this->refresh();
                }
            }
            
            $fields = Field::modelsWhere('id_spravka = ?', array($product->id));
            $field = new Field();
            if (Field::model((int) $id_field)) {
                $field = Field::model((int) $id_field);
            }
            $field->id_spravka = $product->id;
            if (isset($_POST['field'])) {
                $field->__attributes = $_POST['field'];
                if ($field->save()) {
                    $this->refresh();
                }
            }
        }
        $this->render('additional', array('spravka' => $product, 'fields' => $fields, 'field' => $field));
    }

    public function actionEditAdditional($id_spravka = 0, $id_field = 0) {
        $spravka = Cpravka::model((int) $id_spravka);
        if ($spravka) {
        
        }
    }

    public function actionDeleteAdditional($id_spravka = 0, $id_field = 0) {
        $spravka = Cpravka::model((int) $id_spravka);
        if ($spravka) {
            Field::delete((int) $id_field);
            $this->redirect('/cp/spravki/additional/' . $spravka->id);
        }
    }

    private function loadImage($catalog = '') {
        $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/uploads/';
        if(!empty($catalog)){
            $uploaddir .= $catalog . '/';
        }
        if(!file_exists($uploaddir)){
            mkdir($uploaddir);
        }
        $uploadfile = $uploaddir . basename($_FILES['userfile']['name']);
                
        if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
            return $_FILES['userfile']['name'];
        } else {
            return false;
        }
    }

    public function actionDelete($id = 0) {
        if (Cpravka::delete((int) $id)) {
            Field::deleteWhere('id_spravka = ?', array((int) $id));
        }
        $this->redirect('/cp/spravki/');
    }

    public function actionEdit($id_spravka = 0) {
        $spravka = Cpravka::model((int) $id_spravka);
        if ($spravka) {
            if (isset($_POST['form'])) {
                $spravka->__attributes = $_POST['form'];
                if ($spravka->save()) {
                    $this->redirect('/cp/spravki/');
                }
            }
            
            $categories = Category::modelsWhere('id ORDER BY id DESC');
            $this->render('edit', array('spravka' => $spravka, 'categories' => $categories));
        } else {
            $this->redirect('/cp/spravki/');
        }
    }

    public function actionEditDublicate($id_spravka = 0) {
        $spravka = Cpravka::model((int) $id_spravka);
        if ($spravka) {
            if (isset($_POST['form'])) {
                $spravka->__attributes = $_POST['form'];
                if ($spravka->save()) {
                    $this->redirect('/cp/spravki/viewdublicate/');
                }
            }
            $this->render('editdublicate', array('spravka' => $spravka));
        } else {
            $this->redirect('/cp/spravki/viewdublicate/');
        }
    }

}
