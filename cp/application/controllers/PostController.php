<?php
class PostController extends Controller{
	static $rules = array(
		'index' => array(
                    'users' => array('guest'),
                    'redirect' => '/cp/index'),
		'qa' => array(
                    'users' => array('admin'),
                    'redirect' => '/cp/index'),
		'review' => array(
                    'users' => array('admin'),
                    'redirect' => '/cp/index'),
		'mi' => array(
                    'users' => array('admin'),
                    'redirect' => '/cp/index'),
		'additional' => array(
                    'users' => array('admin'),
                    'redirect' => '/cp/index'),
		'viewallqa' => array(
                    'users' => array('admin'),
                    'redirect' => '/cp/index'),
		'viewallreview' => array(
                    'users' => array('admin'),
                    'redirect' => '/cp/index'),
		'viewallmi' => array(
                    'users' => array('admin'),
                    'redirect' => '/cp/index'),
		'editqa' => array(
                    'users' => array('admin'),
                    'redirect' => '/cp/index'),
		'editreview' => array(
                    'users' => array('admin'),
                    'redirect' => '/cp/index'),
		'editmi' => array(
                    'users' => array('admin'),
                    'redirect' => '/cp/index'),
		'delete' => array(
                    'users' => array('admin'),
                    'redirect' => '/cp/index')
                );

	public function actionIndex(){
	
	}
        
	public function actionDelete($id = 0, $type){
            if(Post::delete((int)$id)){
                $this->redirect('/cp/post/viewall'.$type);
            }
	}
        
	public function actionQA(){
            $existpost = null;
            $pageadd = null;
            $last = null;
            $message = '';
            if (isset($_POST['form'])) {
                if($_POST['form']['id_page'] != -1){
                    $post = new Post();
//                    $post->go_url = '';
                    $post->__attributes = $_POST['form'];
                    if ($post->save()) {
                        $existpost = Post::modelsWhere('id_page = ?', array($post->id_page));
                        $pageadd = Cpravka::modelWhere('id = ?', array($post->id_page));
                        $last = &$post;
                        unset($_POST['form']); 
                        //header('Location:'.$_SERVER['REQUEST_URI']);
                       // $this->refresh();
                    }
                }else {
                    $message = "Выберите страницу на которой будет размещен вопрос-ответ";
                }
            }
            $pages = Cpravka::modelsWhere('page_type = ?', array('qa_page'));
//            foreach ($pages as $page){
//                $existpost[$page->page_name] = Post::modelsWhere('id_page = ?', array($page->id));
//            }
            $this->render('qa', array('pages'=>$pages,'exists'=>$existpost, 'pageadd'=>$pageadd, 'last'=>$last,'message'=>$message));
	
	}
	public function actionReview(){
            $message = '';
            $existpost = null;
            $pageadd = null;
            $last = null;
            if (isset($_POST['form'])) {
                if($_POST['form']['id_page'] != -1){
                    $post = new Post();
                    $post->__attributes = $_POST['form'];
                    if ($post->save()) {
                        $existpost = Post::modelsWhere('id_page = ?', array($post->id_page));
                        $pageadd = Cpravka::modelWhere('id = ?', array($post->id_page));
                        $last = &$post;
                        unset($_POST['form']); 
                        //header('Location:'.$_SERVER['REQUEST_URI']);
    //                    $this->refresh();
                    }
                }else {
                    $message = "Выберите страницу на которой будет размещен отзыв";
                }
            }
            $pages = Cpravka::modelsWhere('page_type = ?', array('review_page'));
            $this->render('review', array('pages'=>$pages,'exists'=>$existpost, 'pageadd'=>$pageadd, 'last'=>$last,'message'=>$message));
	
	}
	public function actionMI(){
            $message = '';
            if (isset($_POST['form'])) {
                if($_POST['form']['id_page'] != -1){
                    $post = new Post();
                    $post->__attributes = $_POST['form'];
                    if ($post->save()) {
                        $this->redirect('/cp/post/additional/' . $post->id);                    
                    }
                }else {
                    $message = "Выберите страницу на которой будет размещен пост мединфо";
                }
            }
            
            $pages = Cpravka::modelsWhere('page_type = ?', array('mi_page'));
            $this->render('mi', array('pages'=>$pages,'message'=>$message));
	
	}
        
        public function actionAdditional($id=0){
            $post = Post::model((int) $id);
            $page = Cpravka::modelWhere('id = ?', array($post->id_page));
            $exists = Post::modelsWhere('id_page = ?', array($page->id));
            if ($post) {
                if (isset($_FILES['userfile'])) {
                    $img = $this->Image($post->id);
                    if($img){
                        $post->img = $img;
                        $post->save();
                        $this->refresh();
                    }
                }
               $this->render('additional',array('post'=>$post,'page'=>$page,'exists'=>$exists));
            }
        }
        public function actionEditQA($id=0){
            $post = Post::model((int)$id);
            if($post){
                if(isset($_POST['form'])){
                    $post->__attributes = $_POST['form'];
                    $post->save();
                    $this->redirect('/cp/post/viewallqa');
                }
                $postpage =  Cpravka::modelWhere('id = ?', array($post->id_page));
                $pages = Cpravka::modelsWhere('page_type = ?', array($postpage->page_type));
                $this->render('editqa', array('post'=>$post,'pages'=>$pages));
            }
        }
        public function actionEditReview($id=0){
            $post = Post::model((int)$id);
            if($post){
                if(isset($_POST['form'])){
                    $post->__attributes = $_POST['form'];
                    $post->save();
                    $this->redirect('/cp/post/viewallreview');
                }
                $postpage =  Cpravka::modelWhere('id = ?', array($post->id_page));
                $pages = Cpravka::modelsWhere('page_type = ?', array($postpage->page_type));
                $this->render('editreview', array('post'=>$post,'pages'=>$pages));
            }
        }
        public function actionEditMI($id=0){
            $post = Post::model((int)$id);
            if($post){
                if(isset($_POST['form'])){
                    $post->__attributes = $_POST['form'];
                    $post->save();
                    $this->redirect('/cp/post/viewallmi');
                }
                $postpage =  Cpravka::modelWhere('id = ?', array($post->id_page));
                $pages = Cpravka::modelsWhere('page_type = ?', array($postpage->page_type));
                $this->render('editmi', array('post'=>$post,'pages'=>$pages));
            }
        }
        
        public function actionViewAllQA(){  
            $exists = array();
            $pages = Cpravka::modelsWhere('page_type = ?', array('qa_page'));
            foreach ($pages as $page){
                $exists[$page->id] = Post::modelsWhere('id_page = ? ORDER BY id DESC', array($page->id));
            }
               
            $this->render('viewqa',array('exists'=>$exists));
        }
        
        public function actionViewAllReview(){
            $exists = array();
            $pages = Cpravka::modelsWhere('page_type = ?', array('review_page'));
            foreach ($pages as $page){
                $exists[$page->id] = Post::modelsWhere('id_page = ? ORDER BY id DESC', array($page->id));
            }
            $this->render('viewreview',array('exists'=>$exists));
        }
        
        public function actionViewAllMI(){
            $exists = array();
            $pages = Cpravka::modelsWhere('page_type = ?', array('mi_page'));
            foreach ($pages as $page){
                $exists[$page->id] = Post::modelsWhere('id_page = ? ORDER BY id DESC', array($page->id));
            }
            $this->render('viewmi',array('exists'=>$exists));
        }
        
        private function Image($catalog = '') {
        $uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/uploads/post/';
        if(!empty($catalog)){
            $uploaddir .= $catalog . '/';
        }
        if(!file_exists($uploaddir)){
            mkdir($uploaddir, 0777, true);
        }
        $uploadfile = $uploaddir . basename($_FILES['userfile']['name']);
                
        if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
            return $_FILES['userfile']['name'];
        } else {
            return false;
        }
    }
        
}