<?php
class LoginController extends Controller{
	static $rules = array(
		'index' => array(
			'users' => array('guest'),
			'redirect' => '/cp/index'));

	function actionIndex(){
		$this->layout = 'base';
        $error = '';
        if (isset($_POST['form'])) {
            $login = $_POST['form']['login'];
            $password = $_POST['form']['password'];

            $user = User::modelWhere('login = ? AND password = ?', array($login, $password));
            
//            debug($user);
            
            if ($user) {
                
                $token = Auth::generateToken();
                $user->auth_token = $token;
                
                if ($user->save()) {
                    Auth::setCookie('auth_token', $token);
                    $this->redirect('/cp');
                }
            } else {
                $error = 'Указанная пара логин \ пароль не найдена';
            }
        }
        $this->render('login', array('error' => $error));
	}
}