<?php
class CategoryController extends Controller {
	static $rules = array(
		'index' => array(
			'users' => array('admin'),
			'redirect' => '/cp/login'),
		'edit' => array(
			'users' => array('admin'),
			'redirect' => '/cp/login'),
		'delete' => array(
			'users' => array('admin'),
			'redirect' => '/cp/login'),
		);

	public function actionIndex($id = 0){
		$category = Category::model((int)$id);
		if(!$category){
			$category = new Category();
		}
		if(isset($_POST['form'])){
			$category->__attributes = $_POST['form'];
			$category->save();
			$this->redirect('/cp/category');
		}

		$categories = Category::modelsWhere('id ORDER BY id DESC');
		$this->render('index', array('category'=>$category, 'cats'=>Category::modelsWhere('id ORDER BY id DESC')));
	}

	public function actionDelete($id = 0){
		Category::delete((int) $id);
		$this->redirect('/cp/category');

	}

}