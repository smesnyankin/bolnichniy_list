<?php

class AdminController extends Controller {

    static $rules = array(
        'index' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'
        )
    );

    public function actionIndex() {
        $user = App::gi()->user;
        $message = '';
        if (isset($_POST['form'])) {
            if ($_POST['form']['password'] === $_POST['password_again']) {
                $user->password = $_POST['form']['password'];
                if($user->save()){
                    $message = 'Пароль изменен<br>';
                }
                
            } else {
                $message = 'Пароли не совпадают<br>';
            }
        }
        $this->render('index', array('message' => $message));
    }

}
