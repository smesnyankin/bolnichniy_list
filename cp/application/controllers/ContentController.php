<?php

class ContentController extends Controller {

    static $rules = array(
        'index' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'
        ),
        'create' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'
        ),
        'list' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'
        ),
        'edit' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'
        ),
        'faq' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'
        ),
        'faqdel' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'
        ),
        'view' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'
        ),
        'delete' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'
        )
    );

    public function actionIndex() {
        $this->redirect('/cp/content/list');
    }

    public function actionCreate() {
        $content = new Content();
        if (isset($_POST['form'])) {
            $content->__attributes = $_POST['form'];
            $content->mod_time = time();
            if ($content->save()) {
                $article_page = Page::modelWhere('url = ?', array('stati'));
                if ($article_page) {
                    $article_page->mod_time = time();
                    $article_page->save();
                }
                $this->redirect('/cp/content/list/' . $content->id);
            }
        }
        $categoies = Category::modelsWhere('id ORDER BY id DESC');
        $this->render('index', array('categories' => $categoies, 'content' => $content));
    }

    public function actionView($id = 0) {
        $content;
        if ($id !== 0) {
            $content = Content::model((int) $id);
        }
        $this->render('view', array('content' => $content));
    }

    public function actionList() {
        $categories = Category::modelsWhere('id ORDER BY id DESC');

        if (count($categories)) {
            foreach ($categories as $category) {
                $category->articles = Content::modelsWhere('id_category = ? ORDER BY id DESC', array($category->id));
            }
        }
        $this->render('list', array('categories' => $categories));
    }

    public function actionEdit($id) {
        $content = Content::model((int) $id);
        if ($content) {
            if (isset($_POST['form'])) {
                $content->__attributes = $_POST['form'];
                $content->mod_time = time();
                if ($content->save()) {
                    $article_page = Page::modelWhere('url = ?', array('stati'));
                    if ($article_page) {
                        $article_page->mod_time = time();
                        $article_page->save();
                    }
                    $this->redirect('/cp/content/list');
                }
            }
            $categories = Category::modelsWhere('id ORDER BY id DESC');
            $this->render('index', array('content' => $content, 'categories' => $categories));
        }
    }

    public function actionDelete($id) {
        if (Content::delete((int) $id)) {
            $article_page = Page::modelWhere('url = ?', array('stati'));
            if ($article_page) {
                $article_page->mod_time = time();
                $article_page->save();
            }
        }
        $this->redirect('/cp/content/list');
    }

    public function actionFaq($id = 0) {
        $faq = null;
        if ($id !== 0) {
            $id = (int) $id;
            $faq = Faq::model($id);
            if (!$faq) {
                $faq = new Faq();
            }
        } else {
            $faq = new Faq();
        }

        $faqs = Faq::modelsWhere('id ORDER BY id DESC');
        if (isset($_POST['form'])) {
            $faq->__attributes = $_POST['form'];
            if ($faq->save()) {
                $faq_page = Page::modelWhere('url = ?', array('vopros-otvet'));
                if ($faq_page) {
                    $faq_page->mod_time = time();
                    $faq_page->save();
                }
                $this->redirect('/cp/content/faq');
            }
        }
        $this->render('faq', array('faqs' => $faqs, 'faq' => $faq));
    }

    public function actionFaqDel($id = 0) {
        if ($id !== 0) {
            if (Faq::delete((int) $id)) {
                $faq_page = Page::modelWhere('url = ?', array('vopros-otvet'));
                var_dump($faq_page);
                $faq_page->mod_time = time();
                $faq_page->save();
            }
            $this->redirect('/cp/content/faq');
        }
    }

}
