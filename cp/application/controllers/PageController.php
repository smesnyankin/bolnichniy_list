<?php

class PageController extends Controller {

    static $rules = array(
        'index' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'main' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'about' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'article' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'catalog' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'pay' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'delivery' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'contacts' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'faq' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        
        
        
        
        
        
        
        
        
        
        'static' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'qamain' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'qapage' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'reviewpage' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'mipage' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'viewqamain' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'viewqa' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'viewreview' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'viewmi' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'viewstatic' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'editqamain' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'editqa' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'editreview' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'editmi' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
        'editstatic' => array(
            'users' => array('admin'),
            'redirect' => '/cp/login'),
    );

    
    public function actionStatic() {
        $exists = array();
        $last = null;
        if (isset($_POST['form'])) {
            $page = new Cpravka();
            $page->__attributes = $_POST['form'];
            $page->page_type = 'static';
            if ($page->save()) {
                $last = &$page;
                //$this->refresh();
            }
        }
        $exists = Cpravka::modelsWhere('page_type = ?', array('static'));
        $this->render('static', array('exists'=>$exists,'last'=>$last));
    }
    public function actionQAMain() {
        $exists = array();
        $last = null;
        if (isset($_POST['form'])) {
            $page = new Cpravka();
            $page->__attributes = $_POST['form'];
            $page->page_type = 'qa_main';
            if ($page->save()) {
                $last = &$page;
//                    $this->refresh();
            }
        }
        $exists = Cpravka::modelsWhere('page_type = ?', array('qa_main'));
        $this->render('qamain', array('exists'=>$exists,'last'=>$last));
    }
    public function actionQAPage() {
        $message = '';
        $exists = array();
        $last = null;
        $parent = null;
        if (isset($_POST['form'])) {
            if($_POST['form']['id_qa'] != -1){
                $page = new Cpravka();
                $page->__attributes = $_POST['form'];
                $page->page_type = 'qa_page';
                if ($page->save()) {
                    $parent = Cpravka::modelWhere('id = ?', array($page->id_qa));
                    $exists = Cpravka::modelsWhere('id_qa = ?', array($parent->id));
                    $last = &$page;
                    unset($_POST['form']); 
                    //header('Location:'.$_SERVER['PHP_SELF']);
    //                $this->refresh();
                }
            }else {
                $message = "Выберите категорию на которой будет размещена страница вопрос-ответ";
            }
        }
        
        $categories = Cpravka::modelsWhere('page_type = ?', array('qa_main'));
        $this->render('qapage', array('categories'=>$categories,'last'=>$last,'exists'=>$exists,'parent'=>$parent,'message'=>$message));
    }
    public function actionReviewPage() {
        
        $exists = array();
        $last = null;
        if (isset($_POST['form'])) {
            $page = new Cpravka();
            $page->__attributes = $_POST['form'];
            $page->page_type = 'review_page';
            if ($page->save()) {
                $last = &$page;
//                $this->refresh();
            }
        }
        $exists = Cpravka::modelsWhere('page_type = ?', array('review_page'));
        $this->render('reviewpage', array('exists'=>$exists,'last'=>$last));
    }
    
    public function actionMIPage() {
            
        $exists = array();
        $last = null;
        if (isset($_POST['form'])) {
            $page = new Cpravka();
            $page->__attributes = $_POST['form'];
            $page->page_type = 'mi_page';
            if ($page->save()) {
                $last = &$page;
//                $this->refresh();
            }
        }
        $exists = Cpravka::modelsWhere('page_type = ?', array('mi_page'));
        $this->render('mipage', array('exists'=>$exists,'last'=>$last));
    }
    
    
    public function actionEditStatic($id=0) {
            
        $exists = array();
        $page=null;
        $page = Cpravka::model((int)$id);
        if($page){
            if (isset($_POST['form'])) {
                $page->__attributes = $_POST['form'];
                if ($page->save()) {
                    $this->refresh();
                }
            }
        }
        $exists = Cpravka::modelsWhere('page_type = ?', array('static'));
        $this->render('editstatic', array('page'=>$page,'exists'=>$exists));
    }
    public function actionEditQAMain($id=0) {
        $exists = array();
        $page=null;
        $page = Cpravka::model((int)$id);
        if($page){
            if (isset($_POST['form'])) {
                $page->__attributes = $_POST['form'];
                if ($page->save()) {
                    $this->refresh();
                }
            }
        }
        $exists = Cpravka::modelsWhere('page_type = ?', array('qa_main'));
        $this->render('editqamain', array('page'=>$page,'exists'=>$exists));
    }
    public function actionEditQA($id=0) {
        $exists = array();
        $page=null;
        $pageadd = null;
        
        $page = Cpravka::model((int)$id); 
        if($page){
            if (isset($_POST['form'])) {
                $page->__attributes = $_POST['form'];
                if ($page->save()) {
                    $pageadd = Cpravka::modelWhere('id = ?', array($page->id_qa));
                    $exists = Cpravka::modelsWhere('id_qa = ?', array($pageadd->id));
//                    $this->refresh();
                }
            }
            $categories =  Cpravka::modelsWhere('page_type = ?', array('qa_main'));
            $this->render('editqa', array('categories'=>$categories,'page'=>$page,'pageadd'=>$pageadd,'exists'=>$exists));
        }
    }
    public function actionEditReview($id=0) {
        $exists = array();
        $page=null;
        $page = Cpravka::model((int)$id);
        if($page){
            if (isset($_POST['form'])) {
                $page->__attributes = $_POST['form'];
                if ($page->save()) {
                    $this->refresh();
                }
            }
        }
        $exists = Cpravka::modelsWhere('page_type = ?', array('review_page'));
        $this->render('editreview', array('page'=>$page,'exists'=>$exists));
    }
    public function actionEditMI($id=0) {
        $exists = array();
        $page=null;
        $page = Cpravka::model((int)$id);
        if($page){
            if (isset($_POST['form'])) {
                $page->__attributes = $_POST['form'];
                if ($page->save()) {
                    $this->refresh();
                }
            }
        }
        $exists = Cpravka::modelsWhere('page_type = ?', array('mi_page'));
        $this->render('editmi', array('page'=>$page,'exists'=>$exists));
    }
    
    public function actionViewStatic() {
        $pages = Cpravka::modelsWhere('page_type = ? ORDER BY id DESC', array('static'));
        
        $this->render('viewstatic', array('pages'=>$pages));
    }
    public function actionViewQAMain() {
            
        $pages = Cpravka::modelsWhere('page_type = ? ORDER BY id DESC', array('qa_main'));
        
        $this->render('viewqamain', array('pages'=>$pages));
    }
    public function actionViewQA() {
        $pages = array();
        $pagesmain = array();
        
        $pagesmain = Cpravka::modelsWhere('page_type = ? ORDER BY id DESC', array('qa_main'));
        foreach ($pagesmain as $page){
            $pages[$page->id] = Cpravka::modelsWhere('id_qa = ?', array($page->id));
        }
        
        $this->render('viewqa', array('exists'=>$pages,'main'=>$pagesmain));
    }
    public function actionViewReview() {
       
        $pages = Cpravka::modelsWhere('page_type = ? ORDER BY id DESC', array('review_page'));
        
        $this->render('viewreview', array('pages'=>$pages));
    }
    public function actionViewMI() {
            
        $pages = Cpravka::modelsWhere('page_type = ? ORDER BY id DESC', array('mi_page'));
        
        $this->render('viewmi', array('pages'=>$pages));
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    function actionMain() {
        $page = Page::modelWhere('url = ?', array('index'));
        if (!$page) {
            $page = new Page();
            $page->url = 'index';
            if (!$page->save()) {
                $this->redirect('/cp/error/404');
            }
        }

        if (isset($_POST['form'])) {
            $page->__attributes = $_POST['form'];
            $page->mod_time = time();
            if ($page->save()) {
                $this->refresh();
            }
        }

        $this->render('main', array('page' => $page));
    }

    function actionAbout() {
        $page = Page::modelWhere('url = ?', array('o-kompanii'));
        if (!$page) {
            $page = new Page();
            $page->url = 'o-kompanii';
            if (!$page->save()) {
                $this->redirect('/o-kompanii');
            }
        }
        if (isset($_POST['form'])) {
            $page->__attributes = $_POST['form'];
            $page->mod_time = time();
            if ($page->save()) {
                $this->refresh();
            }
        }
        $this->render('about', array('page' => $page));
    }

    function actionArticle() {
        $page = Page::modelWhere('url = ?', array('stati'));
        if (!$page) {
            $page = new Page();
            $page->url = 'stati';
            if (!$page->save()) {
                $this->redirect('/cp/error/404');
            }
        }
        if (isset($_POST['form'])) {
            $page->__attributes = $_POST['form'];
            $page->mod_time = time();
            if ($page->save()) {
                $this->refresh();
            }
        }
        $this->render('article', array('page' => $page));
    }

    function actionCatalog() {
        $page = Page::modelWhere('url = ?', array('katalog'));
        if (!$page) {
            $page = new Page();
            $page->url = 'katalog';
            if (!$page->save()) {
                $this->redirect('/cp/error/404');
            }
        }
        if (isset($_POST['form'])) {
            $page->__attributes = $_POST['form'];
            $page->mod_time = time();
            if ($page->save()) {
                $this->refresh();
            }
        }
        $this->render('catalog', array('page' => $page));
    }

    function actionPay() {
        $page = Page::modelWhere('url = ?', array('oplata'));
        if (!$page) {
            $page = new Page();
            $page->url = 'oplata';
            if (!$page->save()) {
                $this->redirect('/cp/error/404');
            }
        }
        if (isset($_POST['form'])) {
            $page->__attributes = $_POST['form'];
            $page->mod_time = time();
            if ($page->save()) {
                $this->refresh();
            }
        }
        $this->render('pay', array('page' => $page));
    }

    function actionDelivery() {
        $page = Page::modelWhere('url = ?', array('dostavka'));
        if (!$page) {
            $page = new Page();
            $page->url = 'dostavka';
            if (!$page->save()) {
                $this->redirect('/cp/error/404');
            }
        }
        if (isset($_POST['form'])) {
            $page->__attributes = $_POST['form'];
            $page->mod_time = time();
            if ($page->save()) {
                $this->refresh();
            }
        }
        $this->render('delivery', array('page' => $page));
    }

    function actionContacts() {
        $page = Page::modelWhere('url = ?', array('kontakty'));
        if (!$page) {
            $page = new Page();
            $page->url = 'kontakty';
            if (!$page->save()) {
                $this->redirect('/cp/error/404');
            }
        }
        if (isset($_POST['form'])) {
            $page->__attributes = $_POST['form'];
            $page->mod_time = time();
            if ($page->save()) {
                $this->refresh();
            }
        }
        $this->render('contacts', array('page' => $page));
    }

    function actionFaq() {
        $page = Page::modelWhere('url = ?', array('vopros-otvet'));
        if (!$page) {
            $page = new Page();
            $page->url = 'vopros-otvet';
            if (!$page->save()) {
                $this->redirect('vopros-otvet');
            }
        }
        if (isset($_POST['form'])) {
            $page->__attributes = $_POST['form'];
            $page->mod_time = time();
            if ($page->save()) {
                $this->refresh();
            }
        }
        $this->render('faq', array('page' => $page));
    }
    

}
