<h2>Справки</h2>
<?php if (count($spravki)): ?>
    <table>
        <tr>
            <td>Название</td>
            <td>Url</td>
            <td>Редактировать</td>
            <!--<td>Удалить</td>-->
        </tr>
        <?php foreach ($spravki as $spravka): ?>
            <tr>
                <td><a href="/<?= $spravka->url; ?>" target="_blank"><?= $spravka->page_name; ?></a></td>
                <td><?= $spravka->url; ?></td>
                <td><a href="/cp/spravki/edit/<?= $spravka->id; ?>">[редактировать]</a></td>
                <!--<td><a href="/cp/spravki/additional/<?//= $spravka->id; ?>">[фото и поля форм]</a></td>-->
                <!--<td><a href="/cp/spravki/delete/<?//=$spravka->id; ?>" onclick="if (confirm('Вы уверены, что хотите удалить справку?'))
                            location.href = '/cp/spravki/delete/<?//= $spravka->id; ?>';" >[удалить]</a></td>-->
            </tr>
        <?php endforeach; ?>
    </table>
<?php else: ?>
    <p>Пока еще не добавлено ни одной справки</p>
<?php endif; ?>
