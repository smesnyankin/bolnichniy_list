<h2>Цены: </h2>

<?php if(count($prices)): ?>
    <table>
        <tr>
            <td>Название</td>
            <td>Цена</td>
            <td>Редактировать</td>
            <!--<td>Удалить</td>-->
        </tr>
        <?php foreach ($prices as $price): ?>
            <tr>
                <td><a href="/spravka/<?= $price->url; ?>" target="_blank"><?= $price->page_name; ?></a></td>
                <td><?= $price->price; ?></td>
                <td><a href="/cp/spravki/edit/<?= $price->id; ?>">[редактировать]</a></td>
                <!--<td><a href="/cp/spravki/additional/<?//= $price->id; ?>">[фото и поля форм]</a></td>-->
                <!--<td><a href="/cp/spravki/delete/<?//=$price->id; ?>" onclick="if (confirm('Вы уверены, что хотите удалить справку?'))
                            location.href = '/cp/spravki/delete/<?//= $price->id; ?>';" >[удалить]</a></td>-->
            </tr>
        <?php endforeach; ?>
    </table>
<?php else: ?>
    <p>Пока еще не добавлено ни одной справки</p>
<?php endif; ?>
