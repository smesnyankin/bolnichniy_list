<?php if ($spravka): ?>
    <h2>Фото и поля форм для "<?= $spravka->page_name; ?>"</h2>
    <!-- Расширенные поля заказа справки:<br> -->
    <h3>Изображение справки</h3>
    <div class="container">
        <?php if ($spravka->img): ?>
            Загруженное изображение:<br>
            <img src="/uploads/<?=$spravka->id.'/'.$spravka->img;?>" width="200" height="200">
        <?php else: ?>
            Нет изображения
        <?php endif; ?>
    </div>
    <div class="container">
        <form action="" enctype="multipart/form-data"  method="POST">
            Загрузка изображения: <input name="userfile" type="file" />
            <input type="submit" value="Загрузить" />
        </form>
    </div>
    
    <h2>Для полей формы "Дата рождения" необходимо указывать URL= data_rojdeniya , а для полей "Округ проживания" URL= okrug_projivaniya </h2>
    
    <h3>Дополнительные поля формы заказов</h3>
    <h4><?= $field->id ? 'Редактироване поля' : 'Новое поле формы'; ?></h4>
    <form method="post">
        Название:<br>
        <input type="text" name="field[title]" value="<?= $field->id ? $field->title : ''; ?>" required>
        Url (только латиница):<br>
        <input type="text" name="field[url]" value="<?= $field->id ? $field->url : ''; ?>" required>
        <input type="submit" value="<?= $field->id ? 'Сохранить' : 'Добавить' ?>">
    </form>
    <?php if (count($fields)): ?>
        <table>
            <tr>
                <td>Название</td>
                <td>url</td>
                <td>редактировать</td>
                <td>удалить</td>
            </tr>
            <?php foreach ($fields as $field): ?>
                <tr>
                    <td><?= $field->title; ?></td>
                    <td><?= $field->url; ?></td>
                    <td><a href="/cp/spravki/additional/<?= $spravka->id; ?>/<?= $field->id; ?>">[редактировать]</a></td>
                    <td><a href="/cp/spravki/deleteadditional/<?= $spravka->id; ?>/<?= $field->id; ?>">[удалить]</a></td>
                </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
<?php endif; ?>