<h2>Редактирование справки</h2>
<p>
    <b>Фото и поля форм</b>: <a href="/cp/spravki/additional/<?=$spravka->id;?>">[редактировать]</a>
</p>
<?php if ($categories): ?>
    <script src="/cp/assets/tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
        tinymce.init({
            selector: "#content",
            language: "ru",
            height: "300",
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                "table contextmenu directionality emoticons paste textcolor responsivefilemanager"
            ],
            toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
            toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
            image_advtab: true,
            external_filemanager_path: "/cp/core/libs/filemanager/",
            filemanager_title: "Responsive Filemanager",
            external_plugins: {"filemanager": "/cp/core/libs/filemanager/plugin.min.js"}
        });
    </script>
    <form method="post">
        meta_title:<br>
	<input type="text" name="form[meta_title]" value="<?=($spravka->meta_title) ? $spravka->meta_title : ''; ?>"><br>
        meta_keywords:<br>
	<input type="text" name="form[meta_keywords]" value="<?=($spravka->meta_keywords) ? $spravka->meta_keywords : ''; ?>"><br>
        meta_description:<br>
	<input type="text" name="form[meta_description]" value="<?=($spravka->meta_description) ? $spravka->meta_description : ''; ?>"><br>
        Url (только латиница):<br>
        <input type="text" name="form[url]" value="<?=$spravka->url;?>" required>
        Категория:<br>
        <select name="form[id_category]">
            <?php foreach ($categories as $category): ?>
                <option <?= $spravka->id_category == $category->id ? 'selected' : ''; ?> value="<?= $category->id; ?>"><?= $category->title; ?></option>
            <?php endforeach; ?>
        </select>
        Название:<br>
        <input type="text" name="form[page_name]" value="<?=$spravka->page_name;?>">
        Цена:<br>
        <input type="text" name="form[price]" value="<?=$spravka->price;?>">
        Название для прайс-листа:<br>
        <input type="text" name="form[price_title]" value="<?=$spravka->price_title;?>" required>
        Описание:<br>
        <textarea id="content" name="form[page_content]"><?=$spravka->page_content;?></textarea>
        <input type="submit" value="Редактировать">
    </form>
<?php else: ?>
    <p>Необходимо создать хотя бы одну категрию</p>
<?php endif; ?>