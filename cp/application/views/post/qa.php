<?php if ($message != '') { ?>
<h2><?=$message; ?></h2>
<?php } ?>
<h2>Добавление поста вопрос-ответ</h2>
<script src="/cp/assets/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
	tinymce.init({
            selector: "#content",
            language: "ru",
            height: 100,
            
		    plugins: [
		         "advlist autolink link image lists charmap print preview hr anchor pagebreak",
		         "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
		         "table contextmenu directionality emoticons paste textcolor responsivefilemanager"
		   ],
		   toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
		   toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
		   image_advtab: true ,
	   
		   external_filemanager_path:"/cp/core/libs/filemanager/",
		   filemanager_title:"Responsive Filemanager" ,
		   external_plugins: { "filemanager" : "/cp/core/libs/filemanager/plugin.min.js"}
        });
</script>
<script type="text/javascript">
	tinymce.init({
            selector: "#cont",
            language: "ru",
            height: 200,
            
		    plugins: [
		         "advlist autolink link image lists charmap print preview hr anchor pagebreak",
		         "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
		         "table contextmenu directionality emoticons paste textcolor responsivefilemanager"
		   ],
		   toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
		   toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
		   image_advtab: true ,
	   
		   external_filemanager_path:"/cp/core/libs/filemanager/",
		   filemanager_title:"Responsive Filemanager" ,
		   external_plugins: { "filemanager" : "/cp/core/libs/filemanager/plugin.min.js"}
        });
</script>
<?php if(count($pages)){ ?>
<form method="post">
	Вопрос:<br>
	<textarea id="content" name="form[question]"><?=(!empty($_POST)) ? $_POST['form']['question'] : ''; ?></textarea><br>
	Ответ:<br>
	<textarea id="cont" name="form[answer]"><?=(!empty($_POST)) ? $_POST['form']['answer'] : ''; ?></textarea><br>
        Страница:<br>
        <select name="form[id_page]" required>
            <option value="-1">Выбрать</option>
            <?php foreach ($pages as $page): ?>
                <option value="<?= $page->id; ?>"><?= $page->page_name; ?></option>
            <?php endforeach; ?>
        </select>
	<input type="submit" value="Сохранить">
</form>
    
    <?php if ($last){ ?>
    <p>Последний добавленый</p>
       <table>
            <tr>
                <td>Вопрос</td>
                <td>Ответ</td>
                <td>Редактировать</td>
                <td>Удалить</td>
            </tr>
            <tr>
                <td><?=$last->question; ?></td>
                <td><?=$last->answer; ?></td>
                <td><a href="/cp/post/editqa/<?= $last->id; ?>">[редактировать]</a></td>
                <td><a href="/cp/post/delete/<?= $last->id. '/qa'; ?>" onclick="if (confirm('Вы уверены, что хотите удалить пост?'))
                            location.href = '/cp/post/delete/<?= $last->id. '/qa'; ?>';">[удалить]</a></td>
            </tr>
       </table>
    <?php } ?>
    <?php if ($exists){ ?>
    <p>Существующие вопросы-ответы, для страницы: <?=$pageadd->page_name ? $pageadd->page_name : ''; ?></p>
        <table>
            <tr>
                <td>Вопрос</td>
                <td>Ответ</td>
                <td>Редактировать</td>
                <td>Удалить</td>
            </tr>
            <?php foreach ($exists as $post){ ?>
            <tr>
                <td><?=$post->question; ?></td>
                <td><?=$post->answer; ?></td>
                <td><a href="/cp/post/editqa/<?= $post->id; ?>">[редактировать]</a></td>
                <td><a href="/cp/post/delete/<?= $post->id. '/qa'; ?>" onclick="if (confirm('Вы уверены, что хотите удалить пост?'))
                            location.href = '/cp/post/delete/<?= $post->id. '/qa'; ?>';">[удалить]</a></td>
            </tr>
            <?php } ?>
        </table>
    <?php } ?>
<?php } else { ?>
    <p>Для добавления вопроса-ответа необходимо создать хотя бы одну страницу вопросов-ответов</p>
<?php } ?>