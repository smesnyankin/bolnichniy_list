<?php if ($message != '') { ?>
<h2><?=$message; ?></h2>
<?php } ?>
<h2>Добавление поста мединфо</h2>
<script src="/cp/assets/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
	tinymce.init({
            selector: "#content",
            language: "ru",
            height: 200,
            
		    plugins: [
		         "advlist autolink link image lists charmap print preview hr anchor pagebreak",
		         "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
		         "table contextmenu directionality emoticons paste textcolor responsivefilemanager"
		   ],
		   toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
		   toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
		   image_advtab: true ,
	   
		   external_filemanager_path:"/cp/core/libs/filemanager/",
		   filemanager_title:"Responsive Filemanager" ,
		   external_plugins: { "filemanager" : "/cp/core/libs/filemanager/plugin.min.js"}
        });
</script>
<?php if(count($pages)){ ?>
<form method="post">
	Заголовок:<br>
	<!--<textarea id="cont" name="form[question]"></textarea><br>-->
	<input type="text" name="form[question]" value="<?=(!empty($_POST)) ? $_POST['form']['question'] : ''; ?>" required>
	Пост:<br>
	<textarea id="content" name="form[answer]"><?=(!empty($_POST)) ? $_POST['form']['answer'] : ''; ?></textarea><br>
	URL на страницу справки:<br>
	<input type="text" name="form[go_url]" value="<?=(!empty($_POST)) ? $_POST['form']['go_url'] : ''; ?>" required>
        Страница:<br>
        <select name="form[id_page]">
            <option value="-1">Выбрать</option>
            <?php foreach ($pages as $page): ?>
                <option value="<?= $page->id; ?>"><?= $page->page_name; ?></option>
            <?php endforeach; ?>
        </select>        
	<input type="submit" value="Сохранить">
</form>
<?php } else { ?>
    <p>Для добавления поста мединфо необходимо создать хотя бы одну страницу мединфо</p>
<?php } ?>