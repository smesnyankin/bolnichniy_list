<h2>Редактирование поста мединфо</h2>
Картиночка:<br>
<?php if ($post->img) { ?>
    <img width="250" height="250" src="/uploads/post/<?=$post->id .'/'.$post->img; ?>">
<?php } else {?>
        <p>Отсутствует</p>
<?php } ?>
<p>
    <b>Фото</b>: <a href="/cp/post/additional/<?=$post->id;?>">[редактировать]</a>
</p>

<?php if ($pages): ?>
    <script src="/cp/assets/tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
        tinymce.init({
            selector: "#content",
            language: "ru",
            height: "100",
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                "table contextmenu directionality emoticons paste textcolor responsivefilemanager"
            ],
            toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
            toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
            image_advtab: true,
            external_filemanager_path: "/cp/core/libs/filemanager/",
            filemanager_title: "Responsive Filemanager",
            external_plugins: {"filemanager": "/cp/core/libs/filemanager/plugin.min.js"}
        });
    </script>
    <script type="text/javascript">
        tinymce.init({
            selector: "#cont",
            language: "ru",
            height: "200",
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                "table contextmenu directionality emoticons paste textcolor responsivefilemanager"
            ],
            toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
            toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
            image_advtab: true,
            external_filemanager_path: "/cp/core/libs/filemanager/",
            filemanager_title: "Responsive Filemanager",
            external_plugins: {"filemanager": "/cp/core/libs/filemanager/plugin.min.js"}
        });
    </script>
    <form action ="" method="post">
        
        Заголовок:<br>
	<textarea id="content" name="form[question]" ><?=($post->question) ? $post->question : ''; ?></textarea><br>
	Пост:<br>
	<textarea id="cont" name="form[answer]" ><?=($post->answer) ? $post->answer : ''; ?></textarea><br>
        URL на справку:<br>
        <input type="text" name="form[go_url]" value="<?=($post->go_url) ? $post->go_url : ''; ?>" required><br>
        Страница:<br>
        <select name="form[id_page]">
            <?php foreach ($pages as $page): ?>
                <option <?= ($post->id_page == $page->id) ? 'selected' : ''; ?> value="<?= $page->id; ?>"><?= $page->page_name; ?></option>
            <?php endforeach; ?>
        </select>
        <input type="submit" value="Редактировать">
    </form>
<?php else: ?>
    <p>Необходимо создать хотя бы одну категрию</p>
<?php endif; ?>