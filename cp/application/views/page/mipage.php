<h2>Добавление страницы мединфо</h2>
<script src="/cp/assets/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
	tinymce.init({
            selector: "#content",
            language: "ru",
            height: 200,
            
		    plugins: [
		         "advlist autolink link image lists charmap print preview hr anchor pagebreak",
		         "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
		         "table contextmenu directionality emoticons paste textcolor responsivefilemanager"
		   ],
		   toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
		   toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
		   image_advtab: true ,
	   
		   external_filemanager_path:"/cp/core/libs/filemanager/",
		   filemanager_title:"Responsive Filemanager" ,
		   external_plugins: { "filemanager" : "/cp/core/libs/filemanager/plugin.min.js"}
        });
</script>
<form method="post">
        meta_title:<br>
	<input type="text" name="form[meta_title]" ><br>
        meta_keywords:<br>
	<input type="text" name="form[meta_keywords]"><br>
        meta_description:<br>
	<input type="text" name="form[meta_description]" ><br>
	URL:<br>
	<input type="text" name="form[url]" required>
	Заголовок:<br>
	<textarea id="content" name="form[page_name]"></textarea><br>
	<input type="submit" value="Сохранить">
</form>

<?php if ($last){ ?>
    <p>Последняя добавленная</p>
       <table>
            <tr>
                <td>Загловок</td>
                <td>URL</td>
                <td>редактировать</td>
                <!--<td>удалить</td>-->
            </tr>
            <tr>
                <td><?=$last->page_name; ?></td>
                <td><?=$last->url; ?></td>
                <td><a href="/cp/page/editmi/<?= $last->id; ?>">[редактировать]</a></td>
                <!--<td>удалить</td>-->
            </tr>
       </table>
    <?php } ?>
    <?php if ($exists){ ?>
        <p>Существующие страницы мединфо: </p>
        <table>
            <tr>
                <td>Загловок</td>
                <td>URL</td>
                <td>редактировать</td>
                <!--<td>удалить</td>-->
            </tr>
            <?php foreach ($exists as $page){ ?>
            <tr>
                <td><?=$page->page_name; ?></td>
                <td><?=$page->url; ?></td>
                <td><a href="/cp/page/editmi/<?= $page->id; ?>">[редактировать]</a></td>
            </tr>
            <?php } ?>
        </table>
    <?php } ?>
