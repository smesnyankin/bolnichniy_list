<h2>Добавление категории вопрос-ответ:</h2>

<script src="/cp/assets/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
	tinymce.init({
            selector: "#content",
            language: "ru",
            height: 100,
            
		    plugins: [
		         "advlist autolink link image lists charmap print preview hr anchor pagebreak",
		         "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
		         "table contextmenu directionality emoticons paste textcolor responsivefilemanager"
		   ],
		   toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
		   toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
		   image_advtab: true ,
	   
		   external_filemanager_path:"/cp/core/libs/filemanager/",
		   filemanager_title:"Responsive Filemanager" ,
		   external_plugins: { "filemanager" : "/cp/core/libs/filemanager/plugin.min.js"}
        });
</script>
<form method="post">
	Заголовок:<br>
	<textarea id="content" name="form[page_name]"></textarea><br>
	URL:<br>
	<input type="text" name="form[url]" required>
	<input type="submit" value="Сохранить">
</form>


<?php if ($last): ?>
    <p>Последняя добавленная</p>
       <table>
            <tr>
                <td>Загловок</td>
                <td>URL</td>
                <td>редактировать</td>
            </tr>
            <tr>
                <td><?=$last->page_name; ?></td>
                <td><?=$last->url; ?></td>
                <td>
                    <a href="/cp/page/editqamain/<?= $last->id; ?>" >[редактировать]</a>
                </td>
            </tr>
       </table>
    <?php endif; ?>
    <?php if (count($exists)): ?>
        <p>Существующие категории вопрос-ответ:</p>
        <table>
            <tr>
                <td>Загловок</td>
                <td>URL</td>
                <td>редактировать</td>
            </tr>
            <?php foreach ($exists as $page){ ?>
            <tr>
                <td><?=$page->page_name; ?></td>
                <td><?=$page->url; ?></td>
                <td><a href="/cp/page/editqamain/<?= $page->id; ?>">[редактировать]</a></td>
            </tr>
            <?php } ?>
        </table>
    <?php endif; ?>