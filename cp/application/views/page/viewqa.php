<?php if (count($main)){ ?>
<h2>Страницы из категории:</h2>
<?php if (count($exists)): ?>
        <?php foreach ($exists as $k=>$v): ?>
        <h2><?=Cpravka::getName($k); ?></h2>
        
        <table>
            <tr>
                <td>Заголовок</td>
                <td>URL</td>
                <td>Редактировать</td>
            </tr>
            <?php foreach ($v as $page){ ?>
            <tr>
                <td><?= $page->page_name; ?></td>
                <td><?= $page->url; ?></td>
                <td><a href="/cp/page/editqa/<?= $page->id; ?>">[редактировать]</a></td>
            </tr>
            <?php } ?>
        </table>
        <?php endforeach; ?>
<?php else: ?>
    <p>Пока еще не добавлено ни одной страницы</p>
<?php endif; ?>
<?php } else { ?>
    <p>Нет ни одной категории вопросов-ответов</p>
<?php } ?>