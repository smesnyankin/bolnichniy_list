<?php if ($message != '') { ?>
<h1><?=$message; ?></h1>
<?php } ?>
<h2>Добавление страницы вопрос-ответ</h2>
<?php if(count($categories)){ ?>
<script src="/cp/assets/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
	tinymce.init({
            selector: "#content",
            language: "ru",
            height: 100,
            
		    plugins: [
		         "advlist autolink link image lists charmap print preview hr anchor pagebreak",
		         "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
		         "table contextmenu directionality emoticons paste textcolor responsivefilemanager"
		   ],
		   toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
		   toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
		   image_advtab: true ,
	   
		   external_filemanager_path:"/cp/core/libs/filemanager/",
		   filemanager_title:"Responsive Filemanager" ,
		   external_plugins: { "filemanager" : "/cp/core/libs/filemanager/plugin.min.js"}
        });
</script>
<form action ="" method="post">
        meta_title:<br>
	<input type="text" name="form[meta_title]" ><br>
        meta_keywords:<br>
	<input type="text" name="form[meta_keywords]" ><br>
        meta_description:<br>
	<input type="text" name="form[meta_description]" ><br>
	Заголовок:<br>
	<textarea id="content" name="form[page_name]"><?=(!empty($_POST)) ? $_POST['form']['page_name'] : ''; ?></textarea><br>
	URL:<br>
	<input type="text" name="form[url]" value="<?=(!empty($_POST)) ? $_POST['form']['url'] : ''; ?>" required>
        Категория:<br>
        <select name="form[id_qa]">
            <option value="-1">Выбрать</option>
            <?php foreach ($categories as $category): ?>
                <option value="<?= $category->id; ?>"><?= $category->page_name; ?></option>
            <?php endforeach; ?>
        </select>
	<input type="submit" value="Сохранить">
</form>

<?php if ($last): ?>
    <p>Последняя добавленная</p>
       <table>
            <tr>
                <td>Загловок</td>
                <td>URL</td>
                <td>редактировать</td>
            </tr>
            <tr>
                <td><?=$last->page_name; ?></td>
                <td><?=$last->url; ?></td>
                <td>
                    <a href="/cp/page/editqa/<?= $last->id; ?>" >[редактировать]</a>
                </td>
            </tr>
       </table>
    <?php endif; ?>
    <?php if (count($exists)): ?>
        <p>Существующие страницы вопрос-ответ в категории: <?=$parent->page_name ? $parent->page_name : '' ; ?></p>
        <table>
            <tr>
                <td>Загловок</td>
                <td>URL</td>
                <td>редактировать</td>
            </tr>
            <?php foreach ($exists as $page){ ?>
            <tr>
                <td><?=$page->page_name; ?></td>
                <td><?=$page->url; ?></td>
                <td><a href="/cp/page/editqa/<?= $page->id; ?>">[редактировать]</a></td>
            </tr>
            <?php } ?>
        </table>
    <?php endif; ?>
        
<?php } else { ?>
    <p>Для добавления этой страницы необходимо создать хотя бы одну категорию для вопросов - ответов</p>
<?php } ?>