<h2><p>Существующие страницы мединфо:</h2>
<?php if (count($pages)): ?>
        <table>
            <tr>
                <td>Заголовок</td>
                <td>URL</td>
                <td>Редактировать</td>
            </tr>
        <?php foreach ($pages as $page): ?>
            <tr>
                <td><?= $page->page_name; ?></td>
                <td><?= $page->url; ?></td>
                <td><a href="/cp/page/editmi/<?= $page->id; ?>">[редактировать]</a></td>
            </tr>
        <?php endforeach; ?>
        </table>
<?php else: ?>
    <p>Пока еще не добавлено ни одной страницы</p>
<?php endif; ?>
