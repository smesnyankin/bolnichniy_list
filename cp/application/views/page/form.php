<script src="/cp/assets/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
	tinymce.init({
            selector: "#content",
            language: "ru",
            height: 500,
            
		    plugins: [
		         "advlist autolink link image lists charmap print preview hr anchor pagebreak",
		         "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
		         "table contextmenu directionality emoticons paste textcolor responsivefilemanager"
		   ],
		   toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
		   toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
		   image_advtab: true ,
	   
		   external_filemanager_path:"/cp/core/libs/filemanager/",
		   filemanager_title:"Responsive Filemanager" ,
		   external_plugins: { "filemanager" : "/cp/core/libs/filemanager/plugin.min.js"}
        });
</script>
<form method="post">
	meta_title:<br>
	<input type="text" name="form[meta_title]" value="<?=$page->meta_title;?>">
	meta_keywords:<br>
	<input type="text" name="form[meta_keywords]" value="<?=$page->meta_keywords;?>">
	meta_description:<br>
	<input type="text" name="form[meta_description]" value="<?=$page->meta_description;?>">
	Content: <br>
	<textarea id="content" name="form[content]"><?=$page->content;?></textarea><br>
	<input type="submit" value="Сохранить">
</form>